import web
import xml.etree.ElementTree as ET


# Loading file
tree = ET.parse('temperature.xml')
root = tree.getroot()

# Generating routes
urls = (
    '/([ckf])=(.*)', 'temperature',
)
app = web.application(urls, globals())

class temperature:
    def GET(self, unit, temp ):
        if unit == 'c' :
            C = float(temp)
            K = C + 273.15
            F = C *1.8 + 32
            return 'A temperature of %f C equals %f K or %f F' % (C, K, F)

        if unit == 'f' :
            F = float(temp)
            C = F - 32 / 1.8
            K = C + 273.15
            return 'A temperature of %f F equals %f K or %f C' % (F, K, C)

        if unit == 'k':
            K = float(temp)
            C = K-273.15
            F = C*1.8 +32
            return 'A temperature of %f K equals %f C or %f F' % (K, C, F)

if __name__ == "__main__":
    app.run()
